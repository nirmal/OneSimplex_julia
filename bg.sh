#!/bin/bash

for i in Gi/*.png; do
    echo $i
    convert $i -flatten $i
done

for i in output/*.png; do
    echo $i
    convert $i -flatten $i
done
