#!/usr/bin/env julia

using Gadfly

function L(r, d, ϕ)
    ϵ = if abs(ϕ) < π/2
        1
    else
        -1
    end

    squared_diff = r*r - d*d

    return (squared_diff*tan(ϕ))/( ϵ*sqrt(r*r + squared_diff*tan(ϕ)*tan(ϕ)) + r)
end

function nextPosition(Pᵗ, Pᵗ⁻¹, γ, α, β, F_int, F_ext)
    return Pᵗ + (1-γ)*(Pᵗ-Pᵗ⁻¹) + α*F_int + β*F_ext
end

function tangent(Pᵢ₋₁, Pᵢ₊₁)
    distance = Pᵢ₋₁ - Pᵢ₊₁

    return distance / norm(distance)
end

function normal(Pᵢ₋₁, Pᵢ₊₁)
    result = tangent(Pᵢ₋₁, Pᵢ₊₁)
    tmp = deepcopy(result)
    result[1] = -tmp[2]
    result[2] = tmp[1]
    return result
end

function curvature(Pᵢ, Pᵢ₋₁, Pᵢ₊₁)
    a = norm(Pᵢ₋₁ - Pᵢ)
    b = norm(Pᵢ₊₁ - Pᵢ)
    c = norm(Pᵢ₋₁ - Pᵢ₊₁)

    s = (a+b+c)/2

    # Heron's formula
    area = sqrt(s*(s-a)*(s-b)*(s-c))

    return 4*area/(a*b*c)
end

function phi(Pᵢ, Pᵢ₋₁, Pᵢ₊₁)
    AB = Pᵢ - Pᵢ₋₁
    BC = Pᵢ₊₁ - Pᵢ

    return acos(dot(AB, BC) / (norm(AB)*norm(BC)))
end

function orthogonalProjection(Pᵢ, Pᵢ₋₁, Pᵢ₊₁)
    line = Pᵢ₋₁ - Pᵢ₊₁

    return (dot(Pᵢ, line)/dot(line, line))*line
end

function epsilon(Fᵢ, Pᵢ₋₁, Pᵢ₊₁)
    A = Pᵢ₋₁ - Pᵢ₊₁
    B = Fᵢ - Pᵢ₊₁

    return dot(B, A)/(norm(A)*norm(A))
end

function tangentialForce(Fᵢ, ϵ_ref, Pᵢ₋₁, Pᵢ₊₁)
    return ϵ_ref*Pᵢ₋₁ + ϵ_ref*Pᵢ₊₁ - Fᵢ
end

function normalForce(r, ϵ, ϕ, ϕ_ref, normal)
    return (L(r, abs(2ϵ - 1)*r, ϕ_ref) - L(r, abs(2ϵ - 1)*r, ϕ)) * normal
end

function getNeighborIndices(index, size, points)
    prev = if index==1
        size
    else
        index-1
    end

    next = if index==size
        1
    else
        index+1
    end

    return (prev, next)
end

function main()
    points = readdlm("input.txt")

    img = readdlm("image.txt") |> transpose


    #
    ###
    ##### Threshold filter img to reduce artifacts
    ###
    #
    f(x) = if x >= 150
                255
           else
                0
           end

    img = map(f, img)








    p = plot(y=points[:, 2], x=points[:, 1], Geom.path, Geom.point, Theme(background_color=colorant"white"))
    draw(PNG("output/$(dec(0, 4)).png", 5inch, 5inch), p)







    n = size(points)[1]

    prevpos = deepcopy(points)
    nextpos = zeros(size(points))

    for count=1:200
        println("="^75)
        println("Iteration $count")
        println("="^75)


        G_plot = plot(z=img, Geom.contour, Theme(background_color=colorant"white"))
        append!(G_plot.layers, layer(y=prevpos[:, 2], x=prevpos[:, 1], Geom.path, Geom.point))

        for i=1:n

            #
            ###
            ##### Internal Force Calculation
            ###
            #
            B = vec(points[i, :])
            (A_idx, C_idx) = getNeighborIndices(i, n, points)
            A = vec(points[A_idx, :])
            C = vec(points[C_idx, :])

            eps_ref = 0.5

            #k = curvature(B, A, C)
            normalv = normal(A, C)
            angle = pi - phi(B, A, C)

            #C1 continuity
            internalForce = eps_ref*A + (1-eps_ref)*C - B

            #C2 continuity
            (An1_idx, An2_idx) = getNeighborIndices(A_idx, n, points)
            An1 = vec(points[An1_idx, :])
            An2 = vec(points[An2_idx, :])
            phi1 = phi(An1, A, An2)

            (Cn1_idx, Cn2_idx) = getNeighborIndices(C_idx, n, points)
            Cn1 = vec(points[Cn1_idx, :])
            Cn2 = vec(points[Cn2_idx, :])
            phi2 = phi(Cn1, C, Cn2)

            r = norm(A-C)/2

            phi_ref = (angle + pi-phi1 + pi-phi2)/3

            # C2 continuity
            internalForce = eps_ref*A + (1-eps_ref)*C - B + 0.5*L(r, abs(2*eps_ref-1)*r, phi_ref)*normalv



            #
            ###
            ##### External Force calculation
            ###
            #
            beta_grad = 0.5

            radius = 20
            x = B[1]
            y = B[2]

            x = x |> round |> int
            y = y |> round |> int

            x_low = max(1, x-radius)
            x_high = min(size(img)[1], x+radius)

            y_low = max(1, y-radius)
            y_high = min(size(img)[2], y+radius)

            window = img[x_low:x_high, y_low:y_high]

            w = abs(x_high - x_low)/2 |> round |> int
            h = abs(y_high - y_low)/2 |> round |> int
            center = [w, h]

            if !isempty(window)
                tmpmax = window[1]
                closestIdx = 1
                x2, y2 = 1, 1

                for (idx, val) in enumerate(window)
                  if val > tmpmax
                    tmpmax = val
                    x2, y2 = ind2sub(size(window), idx)
                  elseif val == tmpmax
                    tmp = ind2sub(size(window), idx)
                    tmp = [tmp...]

                    if norm(center-tmp) < norm(center-[x2, y2])
                      x2, y2 = tmp
                    end
                  end
                end

            else
                println("Window empty...continuining ==> Position $B")
                return
            end

            if tmpmax == 0
                G_i = B
            else
                G_i = [x2, y2] + [x_low, y_low]
            end

            F_grad = beta_grad*dot((G_i - B), normalv)*normalv

            append!(G_plot.layers, layer( x=[B[1], G_i[1]], y=[B[2], G_i[2]] , Geom.point, Geom.line, Theme(default_color=colorant"red")))

            # if (count <= 8)
            #   alpha, beta, gamma = 0.5, 0.5, 0.8
            # else
            #   alpha, beta, gamma = 0, 1, 0.8
            # end

            if (count <= 8)
              alpha, beta, gamma = 0.8, 0.25, 0.8
            else
              alpha, beta, gamma = 0, 1, 0.8
            end

            nextpos[i, :] = nextPosition(B, vec(prevpos[i,:]), gamma, alpha, beta, internalForce, F_grad)
        end

        p = plot(y=nextpos[:, 2], x=nextpos[:, 1], Geom.path, Geom.point, Theme(background_color=colorant"white"))
        draw(PNG("output/$(dec(count, 4)).png", 5inch, 5inch), p)

        writedlm("mesh/$(dec(count, 4)).txt", nextpos)



        draw(PNG("Gi/$(dec(count, 4)).png", 5inch, 5inch), G_plot)

        prevpos = deepcopy(points)
        points = deepcopy(nextpos)
        nextpos = zeros(size(points))

    end
end

main()
