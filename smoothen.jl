#!/usr/bin/env julia

using Gadfly

function main()
  filenames = readdir("mesh")

  steps = 30

  currentIndex = 1

  for fileIndex in 2:length(filenames)
    currFileName = filenames[fileIndex]
    prevFileName = filenames[fileIndex-1]

    currPoints = readdlm("mesh/$currFileName")
    prevPoints = readdlm("mesh/$prevFileName")

    difference = currPoints - prevPoints

    for i=1:steps
      points = prevPoints + i/steps * difference

      writedlm("smoothened/smoothened_$(dec(currentIndex, 6)).txt", points)

      p = plot(y=points[:, 2], x=points[:, 1], Geom.path, Geom.point, Theme(background_color=colorant"white"),
               Coord.Cartesian(xmin=40,xmax=150, ymin=40,ymax=145))
      draw(PNG("smoothened/img_$(dec(currentIndex, 6)).png", 5inch, 5inch), p)

      currentIndex += 1
    end

  end
end

main()
