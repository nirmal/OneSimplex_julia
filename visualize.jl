#!/usr/bin/env julia

using Gadfly

function main()
  filenames = readdir("splined")

  for fileIndex in 1:length(filenames)
    if isdir("splined/$(filenames[fileIndex])")
      println("Ignored dir $(filenames[fileIndex])")
      continue
    end

    currFileName = filenames[fileIndex]

    points = readdlm("splined/$currFileName")
    #points = points[1:100:end, :]
    points = points[1:10:end, :]
    #println("$points")

    println("Plot: $(filenames[fileIndex])")

    p = plot(y=points[:, 2], x=points[:, 1], Geom.path, Theme(background_color=colorant"white"),
             Coord.Cartesian(xmin=40,xmax=150, ymin=40,ymax=145),
             Guide.xticks(ticks=nothing),
             Guide.yticks(ticks=nothing),
             Guide.xlabel(""),
             Guide.ylabel(""))
    draw(PNG("splined/images/img_$(filenames[fileIndex]).png", 5inch, 5inch), p)
    end

end

main()
